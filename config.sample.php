<?php

/*{{{ v.151005.001 (0.0.2)

    Sample config file for bitbucket hooks.

    Based on 'Automated git deployment' script by Jonathan Nicoal:
    http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

    See README.md and config.sample.php

    ---
    Igor Lilliputten
    mailto: igor at lilliputten dot ru
    http://lilliputtem.ru/

    Ivan Pushkin
    mailto: iv dot pushk at gmail dot com

}}}*/

// Base tool configuration:
$CONFIG = array(
    'gitCommand'       => 'git',                   // Git command, *REQUIRED*
    'repositoriesPath' => '/var/repositories',     // Folder containing all repositories, *REQUIRED*
    'log'              => true,                    // Enable logging, optional
    'logFile'          => 'bitbucket.log',         // Logging file name, optional
    'logClear'         => true,                    // clear log each time, optional
    'verbose'          => true,                    // show debug info in log, optional
    'folderMode'       => 0700,                    // creating folder mode, optional
);


$DEPLOY_PATH = '/var/www/html';

// List of deployed projects:
$PROJECTS = array(
    'tlosbar/automatic-bitbucket-deploy' => array( // The key is a bitbucket.org repository full name *REQUIRED*
        'master' => array(
            'deployPath'  => $DEPLOY_PATH.'/automatic-deployment',     // Path to deploy project, *REQUIRED*
            'postHookCmd' => '',     // command to execute after deploy, optional
        ),
    ),
    'tlosbar/portfolio' => array( // The key is a bitbucket.org repository full name *REQUIRED*
        'production' => array(
            'deployPath'  => '/var/www/jafarmonkey.fr',     // Path to deploy project, *REQUIRED*
            'postHookCmd' => 'yarn install',     // command to execute after deploy, optional
        ),
    )
);